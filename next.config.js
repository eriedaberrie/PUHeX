/** @type {import('next').NextConfig} */
const nextConfig = {
	async rewrites() {
		return [
			{
				source: '/Mystery%20Gift/mysterygift.txt',
				destination: '/api/mystery-gift',
			},
			{
				source: '/Mystery%20Gift/mysterygift124b.txt',
				destination: '/api/mystery-gift',
			},
		]
	},
};

module.exports = nextConfig;
