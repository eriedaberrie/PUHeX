import { NextResponse } from 'next/server';
import { deflateSync } from 'node:zlib';
import * as Ruby from '@/utils/ruby-types';
import * as Uranium from '@/utils/uranium-data';
import makeMarshal from '@/utils/marshal-writer';

interface BasicGift {
	creationDate: Date;
}

interface PokemonGift extends BasicGift {
	kind: 'pokemon';
	pokemon: Uranium.Pokemon;
}

interface ItemGift extends BasicGift {
	kind: 'item';
	quantity: number;
	id: number;
	name: string;
}

type Gift = ItemGift | PokemonGift;

type PokemonData = [Ruby.AnyInt, Ruby.Object];
type ItemData = [Ruby.AnyInt, Ruby.AnyInt, Ruby.String, Ruby.String];
type GiftData = PokemonData | ItemData;

class Gifts {
	// Gifts last for 15 minutes
	private static readonly DURATION = 15 * 60 * 1000;

	private gifts: Set<Gift> = new Set();
	private _stringified: string | null = null;

	addGift(gift: Gift) {
		this.gifts.add(gift);
		this._stringified = null;

		setTimeout(() => {
			this.gifts.delete(gift);
			this._stringified = null;
		}, Gifts.DURATION);
	}

	get stringified(): string {
		return this._stringified ?? (this._stringified = this.getNewStringified());
	}

	private convertGift(gift: Gift): GiftData {
		switch (gift.kind) {
			case 'pokemon':
				return [
					Ruby.Integer.make(0),
					new Ruby.Object('PokeBattle_Pokemon', gift.pokemon),
				];
			case 'item':
				return [
					Ruby.Integer.make(gift.quantity),
					Ruby.Integer.make(gift.id),
					new Ruby.String(gift.name),
					new Ruby.String(''),
				];
		}
	}

	private getNewStringified(): string {
		const giftsAr: Ruby.Array[] = [];
		// HACK: NaN != NaN, so we use it as the gift[0] to bypass all ID collision checks
		this.gifts.forEach(gift => {
			giftsAr.push(new Ruby.Array(([Ruby.Float.NaN] as Ruby.Data[]).concat(this.convertGift(gift))));
		});
		return deflateSync(makeMarshal(new Ruby.Array(giftsAr))).toString('base64');
	}
};

// Just store the gifts in memory, they don't last long anyways (scalability is
// for nerds)
const GIFTS = new Gifts();

export async function POST(request: Request) {
	// const result = await request.json();
	GIFTS.addGift({
		creationDate: new Date(),
		kind: 'item',
		quantity: 5,
		id: 70, // Leftovers
		name: 'Your Mother',
	});

	return NextResponse.redirect(request.url);
}

export function GET() {
	return new NextResponse(GIFTS.stringified);
}
