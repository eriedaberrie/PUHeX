import * as Ruby from './ruby-types';

class MarshalWriter {
	private static readonly INITIAL_BUFFER_LENGTH = 40;

	// Unsafe should be fine, every byte gets filled up (safety is for NERDS)
	buffer = Buffer.allocUnsafe(MarshalWriter.INITIAL_BUFFER_LENGTH);
	private index = 2;
	private readonly symbols: string[] = [];
	private readonly objects: Ruby.Linkable[] = [];

	constructor(
		data: Ruby.Data,
		private readonly limit?: number,
	) {
		this.buffer.writeUInt16BE(0x0408);
		this.serialize(data);

		// Cut off any extra over-allocated bytes
		this.buffer = this.buffer.subarray(0, this.index);
	}

	private serialize(data: Ruby.Data) {
		if (data instanceof Ruby.Nil)
			this.writeChar('0');
		else if (data instanceof Ruby.Boolean)
			this.writeChar(data.value ? 'T' : 'F');
		else if (data instanceof Ruby.Integer)
			this.serializeInt(data.value);
		else if (data instanceof Ruby.Bignum)
			this.serializeBignum(data.value);
		else if (data instanceof Ruby.Float)
			this.serializeFloat(data.value);
		else if (data instanceof Ruby.Symbol)
			this.serializeSymbol(data.value);
		else if (this.maybeSerializeLink(data, this.objects, '@'))
			null; // Only linkable objects below
		else if (data instanceof Ruby.String)
			this.serializeString(data.value);
		else if (data instanceof Ruby.Array)
			this.serializeArray(data.values);
		else if (data instanceof Ruby.Hash)
			this.serializeHash(data.values, data.defaultValue);
		else if (data instanceof Ruby.Object)
			this.serializeObject(data.className, data.values);
	}

	private withLength(length: number, writeAction: () => number) {
		if (this.index + length > this.buffer.length) this.increaseBufferLength();

		const written = writeAction();

		if (written != length)
			throw new RangeError(`Expected ${length} bytes written, but got ${written}.`);

		this.index += length;
	}

	private writeChar(char: string) {
		this.withLength(1, () => this.buffer.write(char, this.index, 1, 'ascii'));
	}

	private writeRawInt(value: number, length: number, signed = false) {
		this.withLength(length, () => {
			return this.buffer[`write${signed ? '' : 'U'}IntLE`](value, this.index, length) - this.index;
		});
	}

	private writeInt(value: number) {
		if (value === 0) {
			this.writeRawInt(0, 1);
			return;
		}

		if (value < 123 && value >= -123) {
			this.writeRawInt(value + (value > 0 ? 5 : -5), 1, true);
			return;
		}

		const abs = Math.abs(value);
		if (abs < Ruby.Integer.MAX_SIZE) {
			const length = Math.min(Math.floor(Math.log2(value) / 8), 1) + 1;
			this.writeRawInt(value > 0 ? length : -length, 1, true);
			this.writeRawInt(abs, length);
			return;
		}

		throw new RangeError(`Value ${value} too large to represent in integer format.`);
	}

	private writeText(text: string, unicode = false) {
		const length = unicode ? new TextEncoder().encode(text).length : text.length;
		this.writeInt(length);
		this.withLength(length, () => this.buffer.write(text, this.index, length, unicode ? 'utf8' : 'ascii'));
	}

	private writePairs(pairs: Map<Ruby.Data, Ruby.Data>) {
		this.writeInt(pairs.size);

		pairs.forEach((value, key) => {
			this.serialize(key);
			this.serialize(value);
		});
	}

	private maybeSerializeLink<T>(data: T, lookup: T[], char: string): boolean {
		for (let i = 0; i < lookup.length; i++) {
			if (data === lookup[i]) {
				this.writeChar(char);
				this.writeInt(i);
				return true;
			}
		}

		lookup.push(data);
		return false;
	}

	private serializeInt(value: number) {
		this.writeChar('i');
		this.writeInt(value);
	}

	private serializeBignum(value: bigint) {
		this.writeChar('l');
		if (value < 0) {
			this.writeChar('-');
			value *= BigInt(-1);
		} else {
			this.writeChar('+');
		}

		const words: number[] = [];
		while ((value >>= BigInt(16)) > 0)
			words.push(Number(value & BigInt(0xffff)));

		this.writeInt(words.length);

		for (const word of words) this.writeRawInt(word, 2);
	}

	private serializeFloat(value: number) {
		this.writeChar('f');

		if (value === Number.POSITIVE_INFINITY)
			this.writeText('inf');
		else if (value === Number.NEGATIVE_INFINITY)
			this.writeText('-inf');
		else if (Number.isNaN(value))
			this.writeText('nan');
		else
			this.writeText(value.toFixed());
	}

	private serializeSymbol(value: string) {
		if (this.maybeSerializeLink(value, this.symbols, ';')) return;

		this.writeChar(':');
		this.writeText(value);
	}

	private serializeString(value: string) {
		const unicode = !!value.match(/[^\u0000-\u007f]/);
		if (unicode) this.writeChar('I');

		this.writeChar('"');
		this.writeText(value, unicode);

		if (unicode) this.writePairs(new Map().set(Ruby.Symbol.make('E'), Ruby.Boolean.TRUE));
	}

	private serializeArray(values: Ruby.Data[]) {
		this.writeChar('[');
		this.writeInt(values.length);
		for (const data of values) this.serialize(data);
	}

	private serializeHash(values: Map<Ruby.Data, Ruby.Data>, defaultValue?: Ruby.Data) {
		this.writeChar(defaultValue === undefined ? '{' : '}');
		this.writePairs(values);

		if (defaultValue !== undefined) this.serialize(defaultValue);
	}

	private serializeObject(className: string, values: {[key: string]: Ruby.Data}) {
		this.writeChar('o');
		this.serializeSymbol(className);

		const vars = new Map();
		for (const key in values) vars.set(Ruby.Symbol.make('@' + key), values[key]);
		this.writePairs(vars);
	}

	private increaseBufferLength() {
		let newLength = this.buffer.length * 2;

		if (this.limit !== undefined) {
			if (this.buffer.length >= this.limit)
				throw new RangeError(`Marshal buffer surpassed size limit (${this.limit} bytes).`);

			if (newLength > this.limit) newLength = this.limit;
		}

		const newBuffer = Buffer.allocUnsafe(newLength);
		this.buffer.copy(newBuffer);
		this.buffer = newBuffer;
	}
}

export default function makeMarshal(data: Ruby.Data, limit?: number) {
	return new MarshalWriter(data, limit).buffer;
}
