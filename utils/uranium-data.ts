import * as Ruby from './ruby-types';

export type Pokemon = {
	timeRecieved: Ruby.AnyInt;
	// Because who can be bothered with timestamps
	form_change_time: Ruby.Nil;
	species: Ruby.AnyInt;

	personalID: Ruby.AnyInt;
	hp: Ruby.AnyInt;
	totalhp: Ruby.AnyInt;
	ev: Ruby.Array;
	iv: Ruby.Array;
	// NOTE: RPGXP is Ruby 1.8.1, so iv.to_s acts as .join
	oiv: Ruby.String;

	trainerID: Ruby.AnyInt;
	ot: Ruby.String;
	otgender: Ruby.AnyInt;
	language: Ruby.Optional<Ruby.AnyInt>;

	happiness: Ruby.AnyInt;
	name: Ruby.String;
	eggsteps: Ruby.AnyInt;
	status: Ruby.AnyInt;
	statusCount: Ruby.AnyInt;
	item: Ruby.AnyInt;
	mail: Ruby.Nil;
	fused: Ruby.Nil;
	ribbons: Ruby.Array;
	moves: Ruby.Array;
	ballused: Ruby.AnyInt;
	exp: Ruby.AnyInt;

	attack: Ruby.AnyInt;
	defense: Ruby.AnyInt;
	speed: Ruby.AnyInt;
	spatk: Ruby.AnyInt;
	spdef: Ruby.AnyInt;

	obtainMap: Ruby.AnyInt;
	obtainText: Ruby.Optional<Ruby.String>;
	obtainLevel: Ruby.AnyInt;

	obtainMode: Ruby.AnyInt;
	hatchedMap: Ruby.AnyInt;

	pokerus?: Ruby.Optional<Ruby.AnyInt>;
	firstmoves?: Ruby.Optional<Ruby.Array>;
	markings?: Ruby.Optional<Ruby.AnyInt>;

	abilityflag?: Ruby.Optional<Ruby.AnyInt>;
	genderflag?: Ruby.Optional<Ruby.AnyInt>;
	shinyflag?: Ruby.Optional<Ruby.Boolean>;
}
