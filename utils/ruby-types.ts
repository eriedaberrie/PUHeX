export class Nil {
	private constructor() { }

	static readonly NIL = new Nil();
}

export class Boolean {
	private constructor(
		public readonly value: boolean,
	) { }

	static readonly TRUE = new Boolean(true);
	static readonly FALSE = new Boolean(false);
	static make(value: boolean): Boolean {
		return value ? Boolean.TRUE : Boolean.FALSE;
	}
}

export class Integer {
	private constructor(
		public readonly value: number,
	) { }

	static readonly MAX_SIZE = 1073741824;
	static make(value: number): AnyInt {
		if (!Number.isInteger(value))
			throw new RangeError(`Value ${value} is not an integer.`);

		if (Math.abs(value) >= Integer.MAX_SIZE)
			return new Bignum(BigInt(value));

		return new Integer(value);
	}
}

export class Bignum {
	constructor(
		public readonly value: bigint,
	) { }
}

export class Float {
	constructor(
		public readonly value: number,
	) { }

	static readonly POSITIVE_INFINITY = new Float(Number.POSITIVE_INFINITY);
	static readonly NEGATIVE_INFINITY = new Float(Number.NEGATIVE_INFINITY);
	static readonly NaN = new Float(Number.NaN);
}

export class Symbol {
	private constructor(
		public readonly value: string,
	) { }

	// It's fine to keep a lookup table since there should be a finite number of
	// unique symbol names
	private static readonly lookup: {[key: string]: Symbol} = {};
	static make(value: string): Symbol {
		return Symbol.lookup[value] ?? (Symbol.lookup[value] = new Symbol(value));
	}
}

export class String {
	constructor(
		public readonly value: string,
	) { }
}

export class Array {
	constructor(
		public readonly values: Data[],
	) { }
}

export class Hash {
	constructor(
		public readonly values: Map<Data, Data>,
		public readonly defaultValue?: Data,
	) { }
}

export class Object {
	constructor(
		public readonly className: string,
		public readonly values: {[key: string]: Data},
	) { }
}

export type AnyInt = Integer | Bignum
export type Linkable = String | Array | Hash | Object;
export type Optional<T extends Data> = T | Nil;
export type Data = Nil | Boolean | Integer | Bignum | Float | String | Symbol | Array | Hash | Object;
